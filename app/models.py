from django.db import models
from django.contrib.auth.models import User
from django.db.models.deletion import CASCADE

# Create your models here.
GENDER_CHOICES = (
    ('laki-laki', 'Laki-laki'),
    ('perempuan', 'Perempuan'),
)

class ExtendedUser(User):
    gender = models.CharField(max_length=10, choices=GENDER_CHOICES)
