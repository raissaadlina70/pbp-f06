// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';

function loadJson(selector) {
    return JSON.parse(document.querySelector(selector).getAttribute('data-json'));
}

const harian = loadJson('#areaChart');
const hari = [];
let kondisi = [];

function createArray(value) {
    let i = 0;
    let n = harian[0].length - 1;
    kondisi = [];
    while (i < 10) {
        kondisi.push(harian[0][n][value].value)
        i++;
        n--;
    }
}

let i = 0;
let n = harian[0].length - 1;
while (i < 10) {
    let str = harian[0][n]['key_as_string'];
    hari.push(str.substr(5, 5));
    i++;
    n--;
}

hari.reverse();
createArray("jumlah_positif");
kondisi.reverse();

let label;
function changeFunc(value) {
    if (value == "p") {
        createArray('jumlah_positif');
        label = "Jumlah Positif";
    } else if (value == "s") {
        createArray('jumlah_sembuh');
        label = "Jumlah Sembuh";
    } else if (value == "m") {
        createArray('jumlah_meninggal');
        label = "Jumlah Meninggal";
    } else if (value == "d") {
        createArray('jumlah_dirawat');
        label = "Jumlah Dirawat";
    }
    let data = [kondisi[0], kondisi[1], kondisi[2], kondisi[3], kondisi[4], kondisi[5], kondisi[6], kondisi[7], kondisi[8], kondisi[9]];
    updateChart(data, label);
}

function number_format(number, decimals, dec_point, thousands_sep) {
  // *     example: number_format(1234.56, 2, ',', ' ');
  // *     return: '1 234,56'
  number = (number + '').replace(',', '').replace(' ', '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? '.' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? ',' : dec_point,
    s = '',
    toFixedFix = function(n, prec) {
      var k = Math.pow(10, prec);
      return '' + Math.round(n * k) / k;
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '').length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1).join('0');
  }
  return s.join(dec);
}

function updateChart(data, label) {
    myLineChart.data.datasets[0].data = data;
    myLineChart.data.datasets[0].label = label;
    myLineChart.update();
}
// Area Chart Example
var ctx = document.getElementById("areaChart");
var myLineChart = new Chart(ctx, {
  type: 'line',
  data: {
    labels: [hari[0], hari[1], hari[2], hari[3], hari[4], hari[5], hari[6], hari[7], hari[8], hari[9]],
    datasets: [{
      label: "Jumlah Positif",
      lineTension: 0.3,
      backgroundColor: "rgba(78, 115, 223, 0.05)",
      borderColor: "rgba(78, 115, 223, 1)",
      pointRadius: 3,
      pointBackgroundColor: "rgba(78, 115, 223, 1)",
      pointBorderColor: "rgba(78, 115, 223, 1)",
      pointHoverRadius: 3,
      pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
      pointHoverBorderColor: "rgba(78, 115, 223, 1)",
      pointHitRadius: 10,
      pointBorderWidth: 2,
      data: [kondisi[0], kondisi[1], kondisi[2], kondisi[3], kondisi[4], kondisi[5], kondisi[6], kondisi[7], kondisi[8], kondisi[9]],
    }],
  },
  options: {
    maintainAspectRatio: false,
    layout: {
      padding: {
        left: 10,
        right: 25,
        top: 25,
        bottom: 0
      }
    },
    scales: {
      xAxes: [{
        time: {
          unit: 'date'
        },
        gridLines: {
          display: false,
          drawBorder: false
        },
        ticks: {
          maxTicksLimit: 7
        }
      }],
      yAxes: [{
        ticks: {
          maxTicksLimit: 5,
          padding: 10,
          // Include a dollar sign in the ticks
          callback: function(value, index, values) {
            return number_format(value);
          }
        },
        gridLines: {
          color: "rgb(234, 236, 244)",
          zeroLineColor: "rgb(234, 236, 244)",
          drawBorder: false,
          borderDash: [2],
          zeroLineBorderDash: [2]
        }
      }],
    },
    legend: {
      display: false
    },
    tooltips: {
      backgroundColor: "rgb(255,255,255)",
      bodyFontColor: "#858796",
      titleMarginBottom: 10,
      titleFontColor: '#6e707e',
      titleFontSize: 14,
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      intersect: false,
      mode: 'index',
      caretPadding: 10,
      callbacks: {
        label: function(tooltipItem, chart) {
          var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
          return datasetLabel + ': ' + number_format(tooltipItem.yLabel);
        }
      }
    }
  }
});
