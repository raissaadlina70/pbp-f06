from django.shortcuts import render, redirect

from .forms import NewUserForm
from django.contrib.auth import login, authenticate, logout, login as auth_login
from django.contrib import messages
from django.contrib.auth.forms import AuthenticationForm
import requests
import json

# Create your views here.
def error_404(request, exception):
    context = {'title': 'Halaman Tidak Ditemukan'}
    return render(request, '404.html', context)


def index(request):
    context = {'title': 'Dashboard'}
    if request.user.is_authenticated:
        context['username'] = request.user.username
    response = requests.get(
        'https://data.covid19.go.id/public/api/update.json').json()
    context['harian'] = json.dumps([response['update']['harian']])
    total = response['update']['total']
    context['created'] = response['update']['penambahan']['created']
    context['positif'] = "{:,}".format(
        int(total['jumlah_positif'])).replace(',', '.')
    context['dirawat'] = "{:,}".format(
        int(total['jumlah_dirawat'])).replace(',', '.')
    context['sembuh'] = "{:,}".format(
        int(total['jumlah_sembuh'])).replace(',', '.')
    context['meninggal'] = "{:,}".format(
        int(total['jumlah_meninggal'])).replace(',', '.')
    response = requests.get(
        'https://data.covid19.go.id/public/api/pemeriksaan-vaksinasi.json').json()
    context['vaksin'] = json.dumps(
        [
            response['vaksinasi']['total']
        ])
    return render(request, 'index.html', context)


def login_request(request):
    context = {'title': 'Login'}
    if request.user.is_authenticated:
        return redirect("/")
    if request.method == "POST":
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            remember = request.POST.get('remember')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                if remember == "on":
                    request.session.set_expiry(60*60*24*30)
                else:
                    request.session.set_expiry(0)
                messages.success(request, "Login berhasil dilakukan.")
                return redirect("/")
        else:
            messages.error(
                request, "Username atau password salah.")
    form = AuthenticationForm()
    context["login_form"] = form
    return render(request, "login.html", context)


def register_request(request):
    context = {'title': 'Register'}
    if request.user.is_authenticated:
        return redirect("/")
    if request.method == "POST":
        form = NewUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            messages.success(request, "Registrasi berhasil.")
            return redirect("/")
        else:
            messages.error(
                request, "Registrasi gagal. Informasi tidak valid.")
    form = NewUserForm()
    context["register_form"] = form
    return render(request, "register.html", context)


def logout_request(request):
    logout(request)
    messages.info(request, "Logout berhasil dilakukan.")
    return redirect("/")


def provinsi(request):
    context = {'title': 'Data Covid-19 per Provinsi'}
    response = requests.get(
        'https://data.covid19.go.id/public/api/prov.json').json()
    for data in response['list_data']:
        data['jumlah_dirawat'] = "{:,}".format(
            int(data['jumlah_dirawat'])).replace(',', '.')
        data['jumlah_sembuh'] = "{:,}".format(
            int(data['jumlah_sembuh'])).replace(',', '.')
        data['jumlah_meninggal'] = "{:,}".format(
            int(data['jumlah_meninggal'])).replace(',', '.')
    context['response'] = response
    response = requests.get(
        'https://data.covid19.go.id/public/api/update.json').json()
    context['created'] = response['update']['penambahan']['created']
    return render(request, 'provinsi.html', context)


def berita(request):
    context = {}
    return render(request, 'berita.html', context)


def forum(request):
    context = {}
    return render(request, 'forum.html', context)
